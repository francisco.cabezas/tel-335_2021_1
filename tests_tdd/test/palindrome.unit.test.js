const palindrome = require('../src/palindrome')

test('oso es palindrome', () => {
    const palindromeCheck = palindrome.isPalindrome('oso')
    expect(palindromeCheck).toBe(true)
})

test('perro no es palindrome', () => {
    const palindromeCheck = palindrome.isPalindrome('perro')
    expect(palindromeCheck).toBe(false)
})

test('"anita lava la tina" es palindrome', () => {
    const palindromeCheck = palindrome.isPalindrome('anita lava la tina')
    expect(palindromeCheck).toBe(true)
})

test('osO es palindrome', () => {
    const palindromeCheck = palindrome.isPalindrome('osO')
    expect(palindromeCheck).toBe(true)
})

test('"a man, a plan, a canal, panama" es palindrome', () => {
    const palindromeCheck = palindrome.isPalindrome('a man, a plan, a canal, panama')
    expect(palindromeCheck).toBe(true)
})

test('null no es palindrome', () => {
    const palindromeCheck = palindrome.isPalindrome(null)
    expect(palindromeCheck).toBe(false)
})
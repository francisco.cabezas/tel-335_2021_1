const palindrome = require('./palindrome')

exports.textValidate = (input, num1, num2) => {
    if(!Number(num1)) return 0
    if(!Number(num2)) return 0

    const palindromeCheck = palindrome.isPalindrome(input)
    const value1 = Number(num1) || 0
    const value2 = Number(num2) || 0
    if(!palindromeCheck) return value1 - value2
    return value1 + value2
}
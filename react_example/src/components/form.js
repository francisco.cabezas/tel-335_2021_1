import { useState } from 'react'
import { Form, Row, Col, Button, Container } from 'react-bootstrap'

/**
 * Form --> Container
 *              |_
 *                  Row -> Col -> Titulo
 *                  Row -> Col -> Email    Col -> Contraseña
 *                  Row -> Col -> Ingresar
 */
function FormComponent() {
    const [email, setEmail] = useState('')

    const showMessage = () => {
        alert('Email: ' + email)
    }

    return (
        <div style={{marginTop:150}}>
            <Form>
                <section id="section">
                    <Container>
                        <Row>
                            <Col>
                                <h2 className="home-title"><strong>Ejemplo de formulario simple</strong></h2>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Form.Group>
                                    <label>E-MAIL:</label>
                                    <br />
                                    <Form.Control id="email" type="email" required onChange={(e) => setEmail(e.target.value)}/>
                                    <br />
                                </Form.Group>
                            </Col>
                            <Col>
                                <Form.Group>
                                    <label>PASSWORD:</label>
                                    <br />
                                    <Form.Control id="password" type="password" required/>
                                    <br />
                                </Form.Group>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Button
                                    type="button"
                                    size="lg"
                                    onClick={showMessage}
                                    style={{backgroundColor:"blueviolet", borderColor:"blueviolet", textTransform:"uppercase"}} >
                                    <label>Ingresar</label>
                                </Button>
                            </Col>
                        </Row>
                        <br />
                        <Row>
                            <Col>
                                <h2>{email}</h2>
                            </Col>
                        </Row>
                    </Container>
                </section>
            </Form>
        </div>
    )
}

export default FormComponent
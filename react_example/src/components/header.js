function header(props) {
    return (
        <div style={{marginTop:50}}>
            <header>
                <nav>
                    <h1>TEL-335 - {props.name}</h1>
                </nav>
            </header>
        </div>
    )
}

export default header
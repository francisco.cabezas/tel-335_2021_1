import Header from './components/header'
import Formulario from './components/form'
import './App.css';

function App() {
  return (
    <div className="App">
      <Header name="Diseño e implementación de Aplicaciones Web y Móvil"/>
      <Formulario />
    </div>
  );
}

export default App;

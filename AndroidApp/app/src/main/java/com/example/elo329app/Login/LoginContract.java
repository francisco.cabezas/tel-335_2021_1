package com.example.elo329app.Login;

public interface LoginContract {
    void onLoginSucceed();
    void onLoginFailed();
}

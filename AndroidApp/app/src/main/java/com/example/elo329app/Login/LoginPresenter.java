package com.example.elo329app.Login;

import com.example.elo329app.Model.UserModel;

public class LoginPresenter {
    private LoginContract loginContract;
    private UserModel userModel;

    public LoginPresenter (LoginContract loginContract) {
        this.loginContract = loginContract;
        this.userModel = (new LoginInteractor()).getMockUser();
    }

    public void manageLoginAccess (String inputEmail, String inputPassword) {
        boolean isValidUser = userModel.validateCredentials(inputEmail, inputPassword);
        if (isValidUser) {
            loginContract.onLoginSucceed();
        } else {
            loginContract.onLoginFailed();
        }
    }
}

package com.example.elo329app.Home;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.example.elo329app.R;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
    }
}

package com.example.elo329app.Login;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.elo329app.R;

public class LoginActivity extends AppCompatActivity implements LoginContract {

    private EditText emailEditText;
    private EditText passEditText;
    private Button loginButton;

    private LoginPresenter loginPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initReferences();
        initActions();
    }

    /**
     * Inicialización de referencias
     */
    private void initReferences () {
        emailEditText = findViewById(R.id.editTextTextEmailAddress);
        passEditText = findViewById(R.id.editTextTextPassword);
        loginButton = findViewById(R.id.loginButton);

        loginPresenter = new LoginPresenter(LoginActivity.this);
    }

    private void initActions () {
        loginButton.setOnClickListener(v -> {
            String userEmail = emailEditText.getText().toString();
            String userPassword = passEditText.getText().toString();

            loginPresenter.manageLoginAccess(userEmail, userPassword);
        });
    }

    @Override
    public void onLoginSucceed() {
        Toast.makeText(LoginActivity.this, getString(R.string.valid_user_label), Toast.LENGTH_LONG).show();
        LoginRouter.goToHome(LoginActivity.this);
    }

    @Override
    public void onLoginFailed() {
        Toast.makeText(LoginActivity.this, getString(R.string.invalid_user_label), Toast.LENGTH_LONG).show();
    }
}

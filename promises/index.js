const { searchUserByIdCallback, searchUserByIdPromise } = require('./data/user')
const { searchBooksByUserNameCallback, searchBooksByUserNamePromise } = require('./data/books')

setTimeout(() => { // Callback Hell
    console.log('Buscando información...')
    searchUserByIdCallback(1, (user) => {
        console.log('Nombre: ' + user.name)
        searchBooksByUserNameCallback(user.name, (books) => {
            console.log(books)
        })
    })
}, 2000)

console.log('----------------------------------------------------')

console.log('Buscando información...')
searchUserByIdPromise(1)
    .then(user => {
        console.log('Nombre: ' + user.name)
        return searchBooksByUserNamePromise(user.name)
    })
    .then(books => { console.log(books) })
    .catch(error => console.log(error))

console.log('----------------------------------------------------')

performSearch = async (id) => {
    try {
        const user = await searchUserByIdPromise(id)
        console.log('Nombre: ' + user.name)

        const books = await searchBooksByUserNamePromise(user.name)
        console.log(books)
    } catch (error) {
        console.log(error)
    }
}

performSearch(1)
exports.searchBooksByUserNamePromise = (name) => {
    const promise = new Promise((resolve, reject) => {
        setTimeout(() => {
            console.log(`Libros encontrados para ${name}:`)
            resolve([
                { id: 12, book: 'El señor de los anillos'},
                { id: 432, book: 'Harry Potter'}
            ])
        }, 2000)
    })
    return promise
}

exports.searchBooksByUserNameCallback = (name, callback) => {
    setTimeout(() => {
        console.log(`Libros encontrados para ${name}:`)
        callback([
            { id: 12, book: 'El señor de los anillos'},
            { id: 432, book: 'Harry Potter'}
        ])
    }, 2000)
}
"use strict";
exports.__esModule = true;
exports.getUser = exports.getUsers = exports.insertUser = void 0;
var UserRepository_1 = require("../repository/UserRepository");
exports.insertUser = function (email, name) {
    return UserRepository_1.insertUserRepository(name, email);
};
exports.getUsers = function () {
    return UserRepository_1.getUsersRepository();
};
exports.getUser = function (email) {
    return UserRepository_1.getUserRepository(email);
};
//# sourceMappingURL=UserController.js.map
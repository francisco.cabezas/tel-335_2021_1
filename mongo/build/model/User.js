"use strict";
exports.__esModule = true;
var mongoose_1 = require("mongoose");
var userSchema = new mongoose_1.Schema({
    email: {
        type: String
    },
    name: {
        type: String
    }
});
exports["default"] = mongoose_1.model('User', userSchema);
//# sourceMappingURL=User.js.map
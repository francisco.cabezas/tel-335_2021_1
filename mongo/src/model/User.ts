import { Schema, model } from 'mongoose'

const userSchema = new Schema({
    email: {
        type: String
    },
    name: {
        type: String
    }
})

export default model('User', userSchema)
import express from 'express'
import bodyParser from 'body-parser'
import mongoose from 'mongoose'

import { insertUser, getUsers, getUser } from './controller/UserController'

const app = express()

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
    extended: true
}))

mongoose.connect('mongodb://mongo:27017/tel335_mongo_db', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true
})

app.get('/', (req, res) => {
    res.json({ message: 'App status OK' })
})

app.post('/insert', async (req, res) => {
    const response = await insertUser(req.body.email, req.body.name)
    res.json(response)
})

app.get('/users', async (req, res) => {
    const response = await getUsers()
    res.json(response)
})

app.get('/users/:email', async (req, res) => {
    const response = await getUser(req.params.email)
    res.json(response)
})

app.listen(4000, () => {
    console.log('Escuchando en el puerto 4000')
})

import { Response } from 'express'
import { JwtPayload } from 'jsonwebtoken'
import { IRequest } from '../jwt/jwt.request.interface'

interface User {
    name: string,
    age: number,
    payload?: (JwtPayload | string)
}

export const getUser = (req: IRequest, res: Response): void => {
    const user: User = {
        name: 'Francisco Cabezas',
        age: 35,
        payload: req.decoded
    }
    res.json(user)
}
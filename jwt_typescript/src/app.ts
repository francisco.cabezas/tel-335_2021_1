import express from 'express'
import { PORT } from './utils/constants'

import { getJWTToken } from './jwt/jwt.controller'
import { getUser } from './user/user.controller'
import { jwtAPIMiddleware } from './jwt/jwt.middleware'

const app = express()

app.set("port", PORT)
app.use(express.json())
app.use(express.urlencoded({ extended: true }))

app.post('/auth/get-token', getJWTToken)

app.get('/user', jwtAPIMiddleware, getUser)

export default app
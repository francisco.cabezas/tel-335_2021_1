import jwt from 'jsonwebtoken'
import { Request, Response } from 'express'
import { API_KEY, API_SECRET, JWT_PRIVATE_KEY, RESPONSE_MESSAGES } from '../utils/constants'

interface IJWTResponse {
    status: boolean,
    message: string
    token?: string
}

export const getJWTToken = (req: Request, res: Response): void => {
    if (areValidRequestHeaders(req)) {
        const token: string = handleCorrectCredentials()
        res.json(responseBuilder(token))
    } else {
        res.json(responseBuilder())
    }
}

function areValidRequestHeaders (req: Request): boolean {
    if (req.body.key === API_KEY && req.body.secret === API_SECRET) return true
    else return false
}

function handleCorrectCredentials(): string {
    return jwt.sign({ appMessage: 'HOLA JWT' }, JWT_PRIVATE_KEY)
}

function responseBuilder(token?: string): IJWTResponse {
    let response: IJWTResponse = {
        status: false,
        message: RESPONSE_MESSAGES.JWT_TOKEN_NOK
    }
    if (token) {
        response.status = true
        response.message = RESPONSE_MESSAGES.JWT_TOKEN_OK
        response.token = token
        return response
    }
    return response
}